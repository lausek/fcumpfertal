<?php

namespace Umpfertal;

require 'vendor/autoload.php';

$teams = Players::load();

if (isset($_GET['team']))
{
    switch ($_GET['team'])
    {
    case 'A':
        // fallthrough
    case 'a':
        View::new()->render('team.html',
            [
                'teams' => [$teams[0]],
            ]
        );

    case 'B':
        // fallthrough
    case 'b':
        View::new()->render('team.html',
            [
                'teams' => [$teams[1]],
            ]
        );
    }
}

View::new()->render('team.html',
    [
        'teams' => $teams, 
    ]
);
