<?php

require 'vendor/autoload.php';

$allowed = [
    'imprint',
    'privacy',
    'stadion',
    'chronik',
    'board',
    '404',
];

if (!in_array($_GET['t'], $allowed))
{
    http_response_code(404);
    exit;
}

Umpfertal\View::new()->render("static/${_GET['t']}.html");
