<?php

namespace Umpfertal;

use pQuery;

final class ExternalData { 

    const VALID_TIME_GAMES = 43200;
    const VALID_TIME_TABLE = 43200;

    private static function load($url)
    {
        $doc = file_get_contents($url);
        return pQuery::parseStr($doc);
    }

    public static function getGames(string $url)
    {
        $cacheKey = crc32($url."games");

        if (self::isFresh($cacheKey, self::VALID_TIME_GAMES))
        {
            return self::getCache($cacheKey); 
        }

        $doc = self::load($url);
        
        $ownTeamName = $doc->query(".verein_headline h1")->html(); 

        $rows = $doc->query(".content_table_std tr");

        $parsedTable = [];
        foreach ($rows as $i => $row)
        {
            // skip header row
            if ($i === 0)
            {
                continue;
            }

            $parsedRow = []; 
            
            $parsedRow['playday'] = $row->query(".team_spielplan_spieltag")->html();
            $parsedRow['date'] = $row->query(".team_spielplan_datum")->html();
            
            $foe = strip_tags($row->query(".team_spielplan_gegner")->html());

            $gametype = $row->query(".team_spielplan_typ span")->html();

            if ($gametype == 'A')
            {
                $parsedRow['home'] = $foe; 
                $parsedRow['guest'] = $ownTeamName; 
            }
            else
            {
                $parsedRow['home'] = $ownTeamName; 
                $parsedRow['guest'] = $foe; 
            }

            $parsedRow['result'] = $row->query(".liga_spieltag_vorschau_datum_content_ergebnis")->html(); 
            $parsedRow['result'] = preg_replace('/\s+/', '', strip_tags($parsedRow['result']));

            $parsedTable[] = $parsedRow;
        }

        self::putCache($cacheKey, $parsedTable);

        return $parsedTable;
    }
    
    public static function getNextGame(string $url)
    {
        $rows = self::getGames($url);
        $today = date("Y-m-d H:i:s");
        
        foreach ($rows as $row)
        {
            $extractedDay = trim(explode(',', $row['date'])[1]);
            $gameDay = date_create_from_format('d.m.y H:i', $extractedDay);
            if ($today < $gameDay)
            {
                break;
            }
        } 

        return $row;
    }

    public static function getTable(string $url): array
    {
        $cacheKey = crc32($url."table");

        if (self::isFresh($cacheKey, self::VALID_TIME_TABLE))
        {
            return self::getCache($cacheKey); 
        }

        $rows = self::load($url)->query(".liga_tabelle tr");

        $parsedTable = [];
        foreach ($rows as $i => $row)
        {
            // skip header row
            if ($i === 0 || $i === count($rows)-1)
            {
                continue;
            }

            $parsedRow = []; 
            
            $parsedRow['place'] = $row->query(".tabelle_nummer")->html();

            $name = explode('(', strip_tags($row->query(".tab_team_name")->html()));
            $parsedRow['name'] = trim($name[0]); 

            $parsedRow['wins'] = $row->query(".tab_wins")->html();
            $parsedRow['draws'] = $row->query(".tab_remis")->html();
            $parsedRow['loses'] = $row->query(".tab_loses")->html();

            $parsedRow['goals'] = $row->query(".tab_goals")->html();
            $parsedRow['diff'] = $row->query(".tab_diff")->html();

            $parsedRow['points'] = strip_tags($row->query(".tab_points")->html());

            $parsedTable[] = $parsedRow;
        }

        self::putCache($cacheKey, $parsedTable);

        return $parsedTable;
    }

    private static function isFresh($name, $lifetime): bool
    {
        $filePath = Config::asAbsolute("/cache/custom/$name");

        if (!file_exists($filePath))
        {
            return false;
        }

        $lastCached = filectime($filePath); 
        $now = time();

        return $now - $lastCached < $lifetime;
    }

    private static function getCache($name)
    {
        $content = file_get_contents(Config::asAbsolute("/cache/custom/$name"));
        return unserialize($content);
    }
    
    private static function putCache($name, $obj)
    {
        $content = serialize($obj);
    
        $dirName = Config::asAbsolute("/cache/custom");

        if (!file_exists($dirName))
        {
            mkdir($dirName, 0777, true);
        }

        file_put_contents("$dirName/$name", $content);
    }

}
