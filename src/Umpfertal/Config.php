<?php

namespace Umpfertal;

final class Config {
    
    const SITE_TITLE = '1. FC Umpfertal';
    const SITE_URL = 'https://www.fcumpfertal.de';

    const LINK_FACEBOOK = 'https://de-de.facebook.com/SG-Umpfertal-752375784921310/';
    const LINK_FUPA = '';
    const LINK_FUSSBALL = '';

    const PATH_POSTDIR = '/data/posts';
    const PATH_DOWNLOADDIR = '/data/download';

    const A_URL_TABLE = 'https://www.fupa.net/club/vfb-boxberg-woelchingen/team/m1/matches';
    const A_URL_GAMES = 'https://www.fupa.net/club/vfb-boxberg-woelchingen/team/m1/matches';

    const B_URL_TABLE = 'https://www.fupa.net/club/tsv-schweigern/team/m1/matches';
    const B_URL_GAMES = 'https://www.fupa.net/club/tsv-schweigern/team/m1/matches';

    public static function new()
    {
        return new Config;
    }

    public static function asAbsolute(string $path): string
    {
        return self::getRoot() . $path;
    }

    public static function getRoot(): string
    {
        return $_SERVER['DOCUMENT_ROOT']; 
    }

    public static function getPlayerFile(): string
    {
        return self::asAbsolute('/data/players.json');
    }

    public static function getPostDir(): string
    {
        return self::asAbsolute(self::PATH_POSTDIR);
    }

    public static function getDownloadDir(): string
    {
        return self::asAbsolute(self::PATH_DOWNLOADDIR);
    }
}
