<?php

namespace Umpfertal;

final class Players {
    
    public static function load()
    {
        $filePath = Config::getPlayerFile();
        return json_decode(file_get_contents($filePath), true);
    }

}
