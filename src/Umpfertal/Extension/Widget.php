<?php

namespace Umpfertal\Extension;

use Umpfertal\ExternalData;

use Twig\TwigFunction;

final class Widget {

    private $cache = NULL;

    public static function new($cache, $view): TwigFunction
    {
        $instance = new self($cache);
        return new TwigFunction('widget',
            function ($team, $type) use ($view)
            {
                switch ($type)
                {
                case 'table-short':
                    return $view->renderPartially('include/widget/table.html', [
                        'table' => ExternalData::getTable(constant("Umpfertal\Config::${team}_URL_TABLE")),
                    ]);

                case 'next-game':
                    return $view->renderPartially('include/widget/game.html', [
                        'game' => ExternalData::getNextGame(constant("Umpfertal\Config::${team}_URL_GAMES")), 
                    ]);

                default:
                    return "error: widget '$type' not found";
                }
            }
        );
    }

    public function __construct($cache)
    {
        $this->cache = $cache;
    }

}
