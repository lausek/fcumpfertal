<?php

namespace Umpfertal;

use Spyc;
use Michelf\Markdown;

final class News {

    const PAGE_COUNT = 10;
    const SHORT_LENGTH = 80;

    private static $articles = NULL;

    private static function load(): array
    {
        $dirPath = Config::getPostDir();
        $articlesRaw = scandir($dirPath, SCANDIR_SORT_DESCENDING);
        $articles = [];

        foreach ($articlesRaw as $articleRaw)
        {
            if (substr($articleRaw, 0, 1) === '.')
            {
                break;
            }
            $articles[] = $articleRaw;
        }

        return $articles;
    }

    private static function parse(string $article): array
    {
        $filePath = Config::getPostDir()."/$article";
        $parsed = Spyc::YAMLLoad($filePath);

        $rawContent = '';
        foreach ($parsed as $key => $val)
        {
            if (is_int($key))
            {
                $rawContent .= $val . "\n\n";
            }
        }

        $parsed['name'] = pathinfo($article, PATHINFO_FILENAME);
        $parsed['date'] = filectime($filePath);
        
        $parsed['content'] = Markdown::defaultTransform($rawContent);

        $parsed['short'] = strip_tags($parsed['content']);
        $parsed['short'] = substr($parsed['short'], 0, self::SHORT_LENGTH-3) . '...';

        if (!array_key_exists('preview', $parsed))
        {
            $fileName = pathinfo($parsed['bild'], PATHINFO_BASENAME);
            $parsed['preview'] = $fileName;

            $handle = @fopen(Config::asAbsolute($parsed['bild']), 'r');
            if ($handle !== false)
            {

                $image = new \Imagick();
                $image->readImageFile($handle);
                
                $imageWidth = $image->getImageWidth(); 
                $imageHeight = $image->getImageHeight(); 
                
                $image->resizeImage($imageWidth*0.5, $imageHeight*0.5, \Imagick::INTERPOLATE_AVERAGE, 1);
                $image->cropImage(400, 400, $imageWidth/4-200, $imageHeight/4-200);
                $image->thumbnailImage(200, 200, true);
                
                $mediumDirname = Config::asAbsolute("/cache/thumbs/medium");
                if (!file_exists($mediumDirname))
                {
                    mkdir($mediumDirname, 0777, true);
                }

                $image->writeImage("$mediumDirname/$fileName");

                $smallDirname = Config::asAbsolute("/cache/thumbs/small");
                if (!file_exists($smallDirname))
                {
                    mkdir($smallDirname, 0777, true);
                }

                $image->writeImage("$smallDirname/$fileName");

                $image->destroy();
                fclose($handle);
            }
        }

        return $parsed;
    }

    private static function parseArray(array $articles): array
    {
        return array_map('self::parse', $articles);
    }

    public static function getPostByName(string $name) //: ?array
    {
        $filePath = Config::getPostDir()."/$name.yaml";
        if (!file_exists($filePath))
        {
            return NULL;
        }
        return self::parse("$name.yaml");
    }

    public static function getPage(int $page): array
    {
        if (self::$articles === NULL)
        {
            self::$articles = self::load();
        }

        if (count(self::$articles) <= self::PAGE_COUNT)
        {
            return self::parseArray(self::$articles);
        }

        $paged = array_chunk(self::$articles, self::PAGE_COUNT);

        if (!array_key_exists($page-1, $paged))
        {
            return [];
        }

        return self::parseArray($paged[$page-1]);
    }

}
