<?php

namespace Umpfertal;

require 'vendor/autoload.php';

$from = $_SERVER['HTTP_REFERER'] ?: "";

$pageIndex = isset($_GET['p']) && 1 <= $_GET['p'] 
                ? (int) $_GET['p']
                : 1;

View::new()
    ->render(
        !isset($_GET['p']) && strpos($from, $_SERVER['SERVER_NAME']) === false
            ? 'landing.html'
            : 'index.html',
        [
            'news' => News::getPage($pageIndex), 
        ]
    );
