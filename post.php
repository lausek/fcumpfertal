<?php

require 'vendor/autoload.php';

Umpfertal\View::new()->render('post.html',
    [
        'post' => Umpfertal\News::getPostByName($_GET['name']),
    ]
);
