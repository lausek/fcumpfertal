#!/usr/bin/python3

from os import path

import sys
import json
import re
import copy

C_POSITIONS = ["Tor", "Verteidigung", "Mittelfeld", "Angriff"]

C_DEFAULT = {
    "team": "",
    "vorname": "",
    "nachname": "",
    "spitzname": "",
    "nummer": "",
    "bild": "",
    "position": "",
}

def clear_screen():
    print("\033[H\033[J")

class Player(dict):
    def __init__(self, base=None):
        dict.__init__(self, base if base else C_DEFAULT)

    def _edit(p=None):
        if not p:
            p = Interactive.search()
            if not p:
                print("Nothing found...")
                input()
                return

        while True:
                clear_screen()
                print("--- Edit ---------\n%s" % str(p))
                a = input("Change value (empty to skip): ")
                if a == '':
                    return
                elif a in p:
                    p[a] = input("%s: " % a)
                else:
                    print("unknown attribute '%s'" % a)

    def _create():
        p = Player()
        for key in p:
            if key[0] == '_':
                continue
            p[key] = input("%s: " % key)

        while True:
            Player._edit(p)
            if Players.instance.create(p):
                break
            else:
                Interactive.confirm()

    def _remove():
        pass

    interactive = {
        "create": _create,
        "edit": _edit,
        "remove": _remove,
    }

    def __str__(self):
        return "\n%s\n" % "\n".join("%s: %s" % (k, self[k]) for k in self if k[0] != '_')

class Players(object):
    instance = None

    def __init__(self, fname):
        self.fname = fname
        self.data = []

        if path.isfile(self.fname):
            try:
                with open(self.fname, "r") as f:
                    self.data = json.load(f)
                    for team in self.data:
                        for pos in C_POSITIONS:
                            for i in range(len(team[pos])):
                                team[pos][i] = Player(team[pos][i])
            except:
                print("the file looks corrupt. please fix this by hand.")
                sys.exit(1)
        else:
            teambase = dict(map(lambda k: (k, []), C_POSITIONS))
            self.data = [copy.deepcopy(teambase), copy.deepcopy(teambase)]
        Players.instance = self

    def teams(self):
        return self.data

    def players(self):
        for team in self.teams():
            for pos in C_POSITIONS:
                for player in team[pos]:
                    yield player

    def create(self, p):
        pos = p["position"]

        if not pos in C_POSITIONS:
            print("position must be one of %s" % str(C_POSITIONS))
            return False

        try:
            t = int(p["team"]) - 1

            if t < 0 or len(self.data) <= t:
                raise ValueError
        except ValueError:
            print("team must be between %d and %d" % (0, len(self.data)-1))
            return False

        self.data[t][pos].append(p)
        return True

    def remove(self, p):
        pass

    def __enter__(self):
        return self

    def __exit__(self, typ, value, traceback):
        print(self.data)
        print("json:")
        print(json.dumps(self.data))

        with open(self.fname, "w") as f:
            json.dump(self.data, f)

        if traceback:
            print(traceback.format_exc())

        return self

class Interactive(object):

    def search():
        def apply_next(it, pool):
            attr = next(it, None)

            if not attr:
                return pool

            give = []

            for entry in pool:
                if re.match(attr[1], entry[attr[0]]):
                    give.append(entry)

            if not give:
                return []

            return apply_next(it, give)

        while True:
            regex = input("Search for regex: ")

            query = list(map(lambda p: p.split("="), regex.split("&")))

            for key, pattern in query:
                if not key in C_DEFAULT:
                    print("Field '%s' is unknown" % key)
                    return None 

                try:
                    re.compile(pattern)
                except re.error:
                    print("Pattern '%s' is invalid" % pattern)
                    return None 

            tmpres = apply_next(iter(query), Players.instance.players())
            op = Interactive.choose(tmpres)
            return tmpres[int(op)] if op != 'q' else None

    def confirm():
        return input("Really? [y/n] ").lower() == 'y';

    def choose(options):
        def putopts():
            print("What would you like to do?")

            for i, option in enumerate(options):
                print("%d. %s" % (i, option))

            print("q. exit")

        while True:
            putopts()

            op = input("> ");

            try:
                if op == '':
                    raise ValueError

                if op.lower() == 'q' or (0 <= int(op) <= len(options)):
                    return op.lower()

            except ValueError:
                print("invalid input")

    def init():
        with Players("../data/players.json") as players:
            standard = [
                ("Create player", Player.interactive["create"]),
                ("Edit player", Player.interactive["edit"]),
                ("Remove player", Player.interactive["remove"]),
            ]

            while True:
                clear_screen()
                op = Interactive.choose(list(map(lambda x: x[0], standard)))

                if op == 'q':
                    if Interactive.confirm():
                        sys.exit()
                else:
                    if hasattr(standard[int(op)][1], '__call__'):
                        standard[int(op)][1]()

if __name__ == '__main__':
        Interactive.init()
