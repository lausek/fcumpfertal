#!/usr/bin/python3

import os
import sys
import zipfile

from datetime import datetime

DIRECTORY = '../data/'
OPTIONS = {
    'save': lambda *args: save(*args), 
    'load': lambda *args: load(*args),
}

def done(msg=None):
    if msg:
        print(msg)
    print("Done...")
    sys.exit(0)

def error(msg):
    print(msg)
    sys.exit(1)

def confirm(msg="Confirm"):
    while True:
        inp = input(msg+" [y/n] ")
        if inp.lower() in ['y', 'j']:
            return True
        elif inp.lower() == 'n':
            return False
        else:
            print("Please choose 'y' or 'n'")

def save(*args):
    def add_dir(zf, path):
        for entry in os.scandir(path):
            if entry.is_dir():
                add_dir(zf, entry.path)
            else:
                print("Writing '%s'" % entry.path)
                zf.write(entry.path)

    fileName = datetime.now().strftime("%Y%m%d%H%M%S.zip")
    with zipfile.ZipFile(fileName, 'w', zipfile.ZIP_DEFLATED) as f:
        for entry in os.listdir(DIRECTORY):
            if os.path.isdir(DIRECTORY + entry):
                os.chdir(DIRECTORY)
                add_dir(f, entry)
            else:
                relName = DIRECTORY + entry
                print("Writing '%s'" % entry)
                f.write(relName, arcname=entry)

    done()

def load(*args):
    if len(args[0]) < 1:
        error("No path was given. Use like: load PATH")

    filePath = args[0][0]
    if not os.path.isfile(filePath):
        error("File '%s' does not exist" % filePath)
    
    try:
        f = zipfile.ZipFile(filePath, 'r', zipfile.ZIP_DEFLATED) 
    except zipfile.BadZipFile:
        error("File '%s' is not a valid zip-file" % filePath)
    
    if not os.path.isdir(DIRECTORY):
        print("Directory '%s' does not exist. Creating..." % DIRECTORY)
        os.mkdir(DIRECTORY)

    if os.listdir(DIRECTORY) and not confirm("Directory is not empty. Overwrite?"):
        done()

    for root, dirs, files in os.walk(DIRECTORY, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))

    with f:
        f.extractall(DIRECTORY)

    done()

def main():
    if len(sys.argv) < 2 or sys.argv[1] not in OPTIONS.keys():
        error("No option specified. Valid options: save | load")
   
    OPTIONS[sys.argv[1]](sys.argv[2:])

if __name__ == '__main__':
    main()
