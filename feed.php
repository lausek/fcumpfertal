<?php

namespace Umpfertal;

require 'vendor/autoload.php';

View::new()->render('feed.xml',
    [
        'news' => News::getPage(1), 
    ]
);
