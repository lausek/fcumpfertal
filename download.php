<?php

namespace Umpfertal;

require 'vendor/autoload.php';

// is there a file we should send to the client?
if (isset($_GET['f']))
{
    $fileName = $_GET['f'];
    $absPath = realPath(Config::getDownloadDir() . "/$fileName");

    // does the file even exist?
    if (!file_exists($absPath))
    {
        http_response_code(404);
        exit;
    }

    // check if file is in download dir (because of ../)
    if (strpos($absPath, Config::getDownloadDir()) !== 0)
    {
        http_response_code(403);
        exit;
    }

    header("Content-Type: " . mime_content_type($absPath));

    $handle = fopen($absPath, 'r');
    if ($handle !== false)
    {
        while (!feof($handle))
        {
            echo fread($handle, 65536);
            flush();
        }
        fclose($handle); 
    }
}
else
{
    $filePaths = array_filter(scandir(Config::getDownloadDir()),
        function ($value)
        {
            // filter all files that have a dot as first char
            return strpos($value, '.') !== 0;
        }
    );

    foreach ($filePaths as $filePath)
    {
        $newFile['link'] = "?f=$filePath";
        $newFile['name'] = pathinfo($filePath, PATHINFO_FILENAME);
        $newFile['type'] = pathinfo($filePath, PATHINFO_EXTENSION);
        $files[] = $newFile;
    }

    View::new()->render('download.html',
        [
            'files' => $files,
        ]
    );
}
